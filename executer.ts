import { AST, ASTType } from './ast.ts';

const output: number[] = [];
const mem: number[] = Array(20).fill(0)
let memIndex = 0;

export default function execute (ast: AST[]): String {
    const memMax = mem.length;
    ast.forEach((element) => {
        switch(element.type) {
            case ASTType.PointerIncrement: 
                if (memIndex < memMax) { memIndex += 1;}
                break;
            case ASTType.PointerDecrement:
                if (memIndex > 0) { memIndex -= 1; }
                break;
            case ASTType.ValueIncrement:
                if (mem[memIndex] < 0xff) { mem[memIndex] += 1; }
                break;
            case ASTType.ValueDecrement:
                if (mem[memIndex] > 0) { mem[memIndex] -= 1 };
                break;
            case ASTType.Loop:
                while(mem[memIndex] > 0) { execute(element.block) };
                break;
            case ASTType.Output:
                output.push(mem[memIndex]);
        }
    });
    return (new TextDecoder).decode(Uint8Array.from(output));
}