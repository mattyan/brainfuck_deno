import parser from './parser.ts';
import execute from './executer.ts';
import { Readable } from 'https://deno.land/std@0.84.0/node/stream.ts';
const reader = Readable.from([...'>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.[-]>++++++++[<++++>-]<.>+++++++++++[<+++++>-]<.>++++++++[<+++>-]<.+++.------.--------.[-]>++++++++[<++++>-]<+.[-]++++++++++.']);
const ast = await parser(reader);
console.log(execute(ast));