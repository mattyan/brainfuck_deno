import { AST, ASTType } from './ast.ts';
import { Readable } from 'https://deno.land/std@0.84.0/node/stream.ts';

export default async function parse(source: &Readable): Promise<AST[]> {
    const ast: AST[] = [];
    while(true) {
        await new Promise((resolve) => { source.once('readable',  resolve); });
        const code = source.read(1);
        if(!code) break;
        switch(code) {
            case '+':
                // インクリメント
                ast.push({
                    type: ASTType.ValueIncrement,
                    block: [],
                });
                break;
            case '-':
                // デクリメント
                ast.push({
                    type: ASTType.ValueDecrement,
                    block: [],
                });
                break;
            case '>':
                // ポインタインクリメント
                ast.push({
                    type: ASTType.PointerIncrement,
                    block: [],
                });
                break;
            case '<':
                // ポインタデクリメント
                ast.push({
                    type: ASTType.PointerDecrement,
                    block: [],
                });
                break;
            case '.':
                // 出力
                ast.push({
                    type: ASTType.Output,
                    block: [],
                });
                break;
            case '[':
                // ループ開始
                ast.push({
                    type: ASTType.Loop,
                    block: await parse(source),
                });
                break;
            case ']':
                // ループ終了
                return ast;
            default:
                break;
        }
    }
    return ast;
}