# brainfuck_deno

[deno](https://deno.land/)でbrainfuckのインタープリターを書いてみた。

## 実行方法
```
deno run index.ts
```

## TODO
- FileStreamからの読み込み
- stdinからの読み込み
    - 一旦全部読み込んでからReadable作っても良いかも