/**
 * 構文木データ
 */

/**
 * タイプ
 */
export enum ASTType {
    None,
    PointerIncrement,
    PointerDecrement,
    ValueIncrement,
    ValueDecrement,
    Loop,
    Output,
};

/**
 * 構文木
 */
export class AST {
    type: ASTType = ASTType.None;
    block: AST[] = [];
};